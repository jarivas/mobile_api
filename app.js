var express = require('express');
var bodyParser = require('body-parser');
var app_routes = require('./routes/app');
var mobile_routes = require('./routes/mobile');
var morgan = require('morgan');
var path = require('path');
var cookieParser = require('cookie-parser')
var appConfig = require('./app_config')

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

console.log('App env is -> ' + app.get('env'));

// if we are not on testing we use morgan to log
if(app.get('env') !== 'test') app.use(morgan('dev'));

app.use(cookieParser());

// allow CORS if required 
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, HEAD, OPTIONS, TRACE");
    next();
});

// expose the DS to the request
app.use(function(req,res,next){
    req.source = appConfig.datasource;
    next();
});

// public path
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

app.use('/api',app_routes);
app.use('/mobile',mobile_routes);

/* [UNPROTECTED Rules] */
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/views/index.html');
});

app.use(express.static(path.join(__dirname, 'views')));

/// catch 404 and forwarding to next error handler
app.use(function(req, res, next) {
    var err = new Error('Not found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;