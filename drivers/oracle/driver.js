/**
 * Created by arivas on 05/06/16.
 */

/* This is the ORACLE driver */

var oracledb = require('oracledb');
var crypto = require('crypto');
var crytpo_config = require('../../app_config.js');

var numRows = 5000;
var sequence = "W_MOBILE_SEQ.NEXTVAL";

/* aux functions for ORACLE driver */
function closeRSandRelease(resultSet,connection) {
    resultSet.close(function(err){
        if (err) {
            console.log(err.message);
        } else {
            doRelease(connection);
        }
    })
}

function doRelease(conn) {
    conn.release(function(err) {
        if(err) {
            console.log(err.message);
        }
    });
}

function fetchRowsFromRS(req, res, next, resultSet, numRows,result) {
    resultSet.getRows( // get numRows rows
        numRows,
        function (err, rows)
        {
            if (err) {
                res.json({'success':false, 'rowCount':0,'message':'Error executing query: ' + err.message});
                return next();
            } else if (rows.length >= 0) {
                result = result.concat(rows);
                if (rows.length === numRows) {
                    return result.concat(fetchRowsFromRS(req,res,next, resultSet, numRows,result));
                } else {
                    if (req.hierarchy_config) {
                        result = transform(req.hierarchy_config, 0, result);
                    }
                    res.json({
                        'success':true,
                        'rowCount': result.length,
                        'rows': result
                    });
                    resultSet.close(function () {
                        return next();
                    });
                }
            }
        });
}

function fetchRowsIntoDict(req,res,next,resultSet,numRows,result) {
    resultSet.getRows( // get numRows rows
    numRows,
    function (err, rows)
    {
        if (err) {
            res.json({'success':false, 'rowCount':0,'message':'Error executing query: ' + err.message});
            return next();
        } else if (rows.length >= 0) {
            result = result.concat(rows);
            if (rows.length === numRows) {
                return result.concat(fetchRowsIntoDict(req,res,next, resultSet, numRows,result));
            } else {
                var dict = {};
                result.map(o => dict[o[req.params.keyColumn]] = o[req.params.valueColumn]);
                res.json({
                    'success':true,
                    'rowCount': result.length,
                    'rows': dict
                });
                resultSet.close(function () {
                    return next();
                });
            }
        }
    });
}

function prepareInsertStmt(postData) {
    var columns = "";
    var values = "";
    var bindValues = {};
    var idColumn = "";
    for (var key in postData) {
        // those columns that have _ID will be treated with the generic sequence to get the next ID
        if (key.indexOf('_ID') > 0 && postData[key] <= 0 && postData[key] != null) {
            columns += ","+key;
            values += ","+sequence;
            idColumn = key;
            bindValues['rid'] = {type:oracledb.NUMBER, dir:oracledb.BIND_OUT};
        } else {
            columns += ","+key;
            values += ",:"+key;
            bindValues[key] = postData[key];
        }
    }
    values = "(" + values.substring(1) + ")";
    columns = "(" + columns.substring(1) + ")";

    console.log({colums:columns, values:values, bindValues:bindValues});
    return {columns:columns, values:values, bindValues:bindValues, idColumn:idColumn}
}

function prepareUpdateStmt(postData) {
    var stmt = "";
    var column = "";
    var bindValues = {};
    for (var key in postData) {
        column = ","+ key + "= :" + key;
        bindValues[key] = postData[key];
        stmt += column;
    }
    stmt = stmt.substring(1);
    return {stmt:stmt, bindValues:bindValues}
}

var driver = {
    establish:function(req,res,next) {

        var source = req.source;
        var connectionDetails = {};
        connectionDetails.user = source.user;
        connectionDetails.connectString = source.host+':'+source.port+'/'+source.SID;
        if (source.encrypted) {
            var decipher = crypto.createDecipher(crytpo_config.alg, crytpo_config.key);
            connectionDetails.password = decipher.update(source.password, 'hex', 'utf8') + decipher.final('utf8');
        } else {
            connectionDetails.password = source.password;
        }
        oracledb.getConnection(
            connectionDetails,
            function(err, connection)
            {
                if (err) {
                    res.json({'success':false, 'rowCount':0,'message':'ERROR connecting to Oracle: ' + err});
                }
                req.connection = connection;
                return next();
            });

    },
    release:function(req) {
        doRelease(req.connection);
    },
    getData:function(req,res,next) {
        req.table = req.params.table;
        req.column = req.params.column;
        req.recordID = req.params.id;
        
        // if no query we just release the connection
        if (typeof(req.table) == 'undefined') {
            return next();
        } else if (req.table.indexOf(' ') > 0 || req.table.length > 30) {
            res.json({'success':false, 'rowCount':0,'message':'table parameter either has spaces or is longer than 30 characters'});
            return next();
        }

        req.logical_query = 'SELECT * from ' + req.table;
        if (req.recordID && req.column) {
            req.logical_query += ' WHERE ' + req.column + ' = ' + req.recordID;
        }
        req.connection.execute(
            req.logical_query,
            [],{resultSet:true,outFormat:oracledb.OBJECT},
            function(err, result)
            {
                if (err) {
                    res.json({'success':false, 'rowCount':0,'message':'Error executing query: ' + err.message});
                    return next();
                }
                fetchRowsFromRS(req,res,next,result.resultSet,numRows,[]);
            });
    },
    getDictionary:function(req,res,next) {
        if (req.params.table.indexOf(' ') > 0 || req.params.table.length > 30) {
            res.json({'success':false, 'rowCount':0,'message':'table parameter either has spaces or is longer than 30 characters'});
            return next();
        } else if (req.params.keyColumn.indexOf(' ') > 0 || req.params.keyColumn.length > 30) {            
            res.json({'success':false, 'rowCount':0,'message':'first column parameter either has spaces or is longer than 30 characters'});
            return next();
        } else if (req.params.valueColumn.indexOf(' ') > 0 || req.params.valueColumn.length > 30) {            
            res.json({'success':false, 'rowCount':0,'message':'first column parameter either has spaces or is longer than 30 characters'});
            return next();
        }
        req.logical_query = 'select distinct '+req.params.keyColumn + ', ' + req.params.valueColumn +  ' from ' + req.params.table;
        req.connection.execute(
            req.logical_query,
            [],{resultSet:true,outFormat:oracledb.OBJECT},
            function(err, result)
            {
                if (err) {
                    res.json({'success':false, 'rowCount':0,'message':'Error executing query: ' + err.message});
                    return next();
                }
                fetchRowsIntoDict(req,res,next,result.resultSet,numRows,[]);
            });
    },
    getNextId:function(req,res,next) {
        req.logical_query = 'select W_MOBILE_SEQ.NEXTVAL from dual';
        req.connection.execute(
            req.logical_query,
            [],{resultSet:true,outFormat:oracledb.OBJECT},
            function(err, result)
            {
                if (err) {
                    res.json({'success':false, 'rowCount':0,'message':'Error executing query: ' + err.message});
                    return next();
                }
                fetchRowsFromRS(req,res,next,result.resultSet,numRows,[]);
            });
    },
    postData:function(req,res,next) {
        req.table = req.params.table;
        if (typeof(req.table) == 'undefined') {
            return next();
        } else if (req.table.indexOf(' ') > 0 || req.table.length > 30) {
            res.json({'success':false, 'rowCount':0,'message':'table parameter either has spaces or is longer than 30 characters'});
            return next();
        }
        var insertStmt = prepareInsertStmt(req.body);

        req.logical_query = "INSERT INTO  " + req.table + " " + insertStmt.columns + " values " +  insertStmt.values;
        if (insertStmt.idColumn) {
            req.logical_query += '  RETURNING ' +insertStmt.idColumn +' into :rid' ;
        }
        console.log(req.logical_query);
        req.connection.execute(
            req.logical_query,
            insertStmt.bindValues,
            function(err, result)
            {
                if (err) {
                    console.error(err.message);
                    res.json({success:false,'message':err.message + ' Query ->' + req.logical_query});
                    return next();
                } else {
                    
                    req.connection.commit(function(){
                    console.log("Rows inserted " + result.rowsAffected);
                    console.log(result.outBinds);
                    res.json({success:true,'message':'row inserted', outBinds:result.outBinds});
                    return next();
                    });
                }
                
            }
        );   
    },
    updateData:function(req,res,next) {
        req.table = req.params.table;
        req.column = req.params.column;
        req.recordID = req.params.id;
        
        // if no query we just release the connection
        if (typeof(req.table) == 'undefined') {
            return next();
        } else if (req.table.indexOf(' ') > 0 || req.table.length > 30) {
            res.json({'success':false, 'rowCount':0,'message':'table parameter either has spaces or is longer than 30 characters'});
            return next();
        } else if (!req.column || !req.recordID) {
            res.json({'success':false, 'rowCount':0,'message':'UPDATE requires column and recordID'});
            return next();
        }

        var updateStmt = prepareUpdateStmt(req.body);

        req.logical_query = 'UPDATE ' + req.table + ' SET ' + updateStmt.stmt;
        if (req.recordID && req.column) {
            req.logical_query += ' WHERE ' + req.column + ' = ' + req.recordID;
        }  
        req.connection.execute(
            req.logical_query,
            updateStmt.bindValues,
            function(err, result)
            {
                if (err) {
                    console.error(err.message);
                    res.json({success:false,'message':err.message + ' Query ->' + req.logical_query});
                    return next();
                }
                else {
                    req.connection.commit(function(){
                    console.log("Records updated: " + result.rowsAffected);
                    res.json({success:true,'message':'records updated'});
                    return next();
                    });
                }
                
            }
        );   
    },
    deleteData:function(req,res,next) {
        req.table = req.params.table;
        req.column = req.params.column;
        req.recordID = req.params.id;
        
        // if no query we just release the connection
        if (typeof(req.table) == 'undefined') {
            return next();
        } else if (req.table.indexOf(' ') > 0 || req.table.length > 30) {
            res.json({'success':false, 'rowCount':0,'message':'table parameter either has spaces or is longer than 30 characters'});
            return next();
        }

        req.logical_query = 'DELETE FROM ' + req.table;
        if (req.recordID && req.column) {
            req.logical_query += ' WHERE ' + req.column + ' = ' + req.recordID;
        }        
        req.connection.execute(
            req.logical_query,
            [], //no bind variables
            function(err, result)
            {
                if (err) {
                    res.json({'success':false, 'rowCount':0,'message':'Error executing query: ' + err.message});
                    return next();
                } else {
                req.connection.commit(function(){
                  console.log("Rows deleted " + result.rowsAffected);
                  res.json({success:true,'message':'row deleted'});
                  return next();
                });
            }
            });
    }
};

module.exports = driver;
