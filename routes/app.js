var express = require('express');
var router = express.Router();

router.get('/testMe', function(req,res) {
    res.render('index', { title: 'Testing 1,2,3...', subtitle: '... all good!' });
});

module.exports = router;