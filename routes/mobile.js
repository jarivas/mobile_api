var express = require('express');
var router = express.Router();

/* driver configuration */

var drivers = {};
drivers['ORACLE'] = require('../drivers/oracle/driver.js');

var establish = function(req,res,next) {
    drivers['ORACLE'].establish(req,res,next);
}

var release = function(req) {
    drivers['ORACLE'].release(req);
}

var getData = function(req,res,next) {
    drivers['ORACLE'].getData(req,res,next);
}

var getNextId = function(req,res,next) {
    drivers['ORACLE'].getNextId(req,res,next);
}

var postData = function(req,res,next) {
    drivers['ORACLE'].postData(req,res,next);
}

var updateData = function(req,res,next) {
    drivers['ORACLE'].updateData(req,res,next);
}

var deleteData = function(req,res,next) {
    drivers['ORACLE'].deleteData(req,res,next);
}

var getDictionary = function(req,res,next) {
    drivers['ORACLE'].getDictionary(req,res,next);
}

router.get('/table/:table', establish, getData,release);
router.get('/table/:table/:column/:id', establish, getData,release);
router.get('/table/dict/:table/:keyColumn/:valueColumn', establish,getDictionary, release);
router.get('/nextId',establish, getNextId, release);
router.post('/table/:table',establish,postData,release);
router.put('/table/:table/:column/:id', establish, updateData,release);
router.delete('/table/:table/:column/:id',establish,deleteData,release);

module.exports = router;